import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';
import 'package:todo/Core/Constants/assets_constants.dart';
import 'package:todo/Core/Extensions/context_extensions.dart';
import 'package:todo/Views/LoginPage/View/login_page_view.dart';

class SplashScreen extends StatefulWidget {
  SplashScreen({Key? key}) : super(key: key);

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  AssetsConstants _assetsConstants = AssetsConstants.instance;


  @override
  void initState() {
    super.initState();
    Future.delayed(Duration(seconds: 4)).then((value) {
      MaterialPageRoute route = MaterialPageRoute(builder: (context)=>LoginPageView());
      Navigator.of(context).push(route);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: buildBody(context),
    );
  }

  Widget buildBody(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        width: context.screenWidth,
        height: context.screenHeight,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Lottie.asset(_assetsConstants.todoLottiePath),
            ),
            SizedBox(height: 50),
            Container(
              width: 20,
              height: 20,
              child: CircularProgressIndicator(
                strokeWidth: 1,
              ),
            ),
            SizedBox(height: 50),
            Text(
              "Flutter ToDo",
              style: context.textTheme.headline4,
            ),
          ],
        ),
      ),
    );
  }
}
