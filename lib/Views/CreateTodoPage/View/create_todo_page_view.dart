import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_vector_icons/flutter_vector_icons.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:todo/Core/Constants/application_constants.dart';
import 'package:todo/Core/Enums/post_categories.dart';
import 'package:todo/Core/Extensions/context_extensions.dart';
import 'package:todo/Models/UserModel/user_model.dart';
import 'package:todo/Views/CreateTodoPage/ViewModel/create_post_viewmodel.dart';

class CreateTodoPageView extends StatefulWidget {
  CreateTodoPageView({required this.userModel, Key? key}) : super(key: key);

  UserModel userModel;

  @override
  _CreateTodoPageViewState createState() => _CreateTodoPageViewState();
}

class _CreateTodoPageViewState extends State<CreateTodoPageView> {

  CreatePostViewModel _viewModel = CreatePostViewModel();
  ApplicationConstants _applicationConstants = ApplicationConstants.instance;
  GlobalKey<FormState> _formKey = GlobalKey();


  @override
  void initState() {
    super.initState();
    _viewModel.fetchAllUsers();
    _viewModel.setOwner(widget.userModel.email!);
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: buildAppBar(context),
      bottomNavigationBar: InkWell(
        onTap: () async{
          if(_formKey.currentState!.validate()){
            _formKey.currentState!.save();
            await _viewModel.sendTodo(widget.userModel.email!);
            Navigator.pop(context,true);
          }
        },
        child: Ink(
          width: context.screenWidth,
          height: 50,
          color: Colors.deepOrange,
          child: Center(
            child: Text(
              "Gönder",
              style: GoogleFonts.montserrat(
                fontSize: 22,
                color: Colors.white
              ),
            ),
          ),
        ),
      ),
      body: buildBody(),
    );
  }

  Widget buildBody() {
    return Observer(
      builder: (_){
        if(_viewModel.loading){
          return Center(
            child: CircularProgressIndicator(),
          );
        }else{
          return ListView(
            children: [
              SizedBox(height: 20),
              // Content
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  SizedBox(width: 10),
                  Text(
                    "İçerik Girin",
                    style: TextStyle(
                        color: Colors.grey.shade700,
                        fontSize: 19,
                        fontWeight: FontWeight.w500),
                  ),
                  SizedBox(width: 10),
                  Icon(
                    Ionicons.document_text_outline,
                    color: Colors.indigoAccent,
                  ),

                ],
              ),
              SizedBox(height: 20),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 20),
                child: Form(
                  key: _formKey,
                  child: TextFormField(
                    minLines: 5,
                    maxLines: 10,
                    maxLength: 250,
                    autofocus: true,
                    textAlign: TextAlign.center,
                    keyboardType: TextInputType.text,
                    validator: (value){
                      if(value == null)  return "Görev Boş Olamaz";
                      if(value.length>0){
                        return null;
                      }else{
                        return "Görev Boş Olamaz";
                      }

                    },
                    onFieldSubmitted: (value){
                      if(_formKey.currentState!.validate()){
                        _formKey.currentState!.save();
                        print(_viewModel.content);
                      }
                    },
                    onSaved: (value)=> _viewModel.setContent("$value"),
                    style: TextStyle(
                        fontSize: 25,
                        fontWeight: FontWeight.w300,
                        color: Colors.grey.shade700
                    ),

                    decoration: InputDecoration(
                      border: InputBorder.none,
                      hintText: "Görev Yaz...",
                      //hintStyle: TextStyle(fontSize: 22)
                    ),
                  ),
                ),
              ),
              // Category
              Divider(height: 20),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  SizedBox(width: 10),
                  Text(
                    "Kategori Seçin",
                    style: TextStyle(
                        color: Colors.grey.shade700,
                        fontSize: 19,
                        fontWeight: FontWeight.w500),
                  ),
                  SizedBox(width: 10),
                  Icon(
                    MaterialIcons.category,
                    color: Colors.pinkAccent,
                  ),

                ],
              ),
              SizedBox(height: 6),
              Observer(
                  builder: (_)=>ListTile(
                    onTap: ()=>changeCategory(),
                    contentPadding: EdgeInsets.symmetric(horizontal: 10),
                    title: Text("${_applicationConstants.generateCategoryText(_viewModel.postCategories)}"),
                    leading: CircleAvatar(
                      backgroundColor: Colors.grey,
                      child: Icon(
                        MaterialIcons.category,
                        color: Colors.white,
                      ),
                    ),
                    trailing: Container(
                      width: 16,
                      height: 16,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(3),
                          border: Border.all(
                              color: _applicationConstants.generateCategoryColor(_viewModel.postCategories),
                              width: 3
                          )
                      ),
                    ),
                  )
              ),
              SizedBox(height: 20),
              // Görevli
              Divider(height: 20),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  SizedBox(width: 10),
                  Text(
                    "Görevli Seçin",
                    style: TextStyle(
                        color: Colors.grey.shade700,
                        fontSize: 19,
                        fontWeight: FontWeight.w500),
                  ),
                  SizedBox(width: 10),
                  Icon(
                    Icons.person,
                    color: Colors.deepPurpleAccent,
                  ),

                ],
              ),
              SizedBox(height: 6),
              Observer(
                  builder: (_)=>ListTile(
                    onTap: ()=>selectOwner(),
                    contentPadding: EdgeInsets.symmetric(horizontal: 10),
                    title: Text("${_viewModel.owner}"),
                    leading: CircleAvatar(
                      backgroundColor: Colors.grey,
                      child: Text(
                        "${_viewModel.owner.toUpperCase().substring(0,1)}",
                        style: GoogleFonts.roboto(
                            color: Colors.white,
                            fontWeight: FontWeight.bold
                        ),
                      )
                    ),
                  )
              ),
              SizedBox(height: 20),
            ],
          );
        }
      },
    );
  }

  AppBar buildAppBar(BuildContext context) {
    return AppBar(
      title: Text(
          "Görev oluştur",
        style: TextStyle(
            color: Colors.grey.shade700,
            fontSize: 16
        ),
      ),
      centerTitle: true,
      backgroundColor: Colors.white,
      elevation: 0.3,
      leading: IconButton(
        icon: Icon(
          Entypo.cross,
          color: Colors.grey.shade700,
        ),
        onPressed: ()=>Navigator.pop(context),
      ),
    );
  }

  void changeCategory() {
    showModalBottomSheet(
        context: context,
        builder: (context){
          return Column(
            children: [
              SizedBox(height: 20),
              Center(
                child: Text(
                    "Kategori Seçin",
                  style: context.textTheme.headline4,
                ),
              ),
              SizedBox(height: 20),
              ListTile(
                onTap: (){
                  _viewModel.setPostCategories(PostCategories.WORK);
                  Navigator.pop(context);
                },
                title: Text("${_applicationConstants.generateCategoryText(PostCategories.WORK)}"),
                leading: CircleAvatar(
                  backgroundColor: Colors.grey,
                  child: Icon(
                    MaterialIcons.category,
                    color: Colors.white,
                  ),
                ),
                trailing: Container(
                  width: 16,
                  height: 16,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(3),
                      border: Border.all(
                          color: _applicationConstants.generateCategoryColor(PostCategories.WORK),
                          width: 3
                      )
                  ),
                ),
              ),
              SizedBox(height: 10),
              ListTile(
                onTap: (){
                  _viewModel.setPostCategories(PostCategories.PERSONAL);
                  Navigator.pop(context);

                },
                title: Text("${_applicationConstants.generateCategoryText(PostCategories.PERSONAL)}"),
                leading: CircleAvatar(
                  backgroundColor: Colors.grey,
                  child: Icon(
                    MaterialIcons.category,
                    color: Colors.white,
                  ),
                ),
                trailing: Container(
                  width: 16,
                  height: 16,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(3),
                      border: Border.all(
                          color: _applicationConstants.generateCategoryColor(PostCategories.PERSONAL),
                          width: 3
                      )
                  ),
                ),
              ),
              SizedBox(height: 10),
              ListTile(
                onTap: (){
                  _viewModel.setPostCategories(PostCategories.ERRANDS);
                  Navigator.pop(context);

                },
                title: Text("${_applicationConstants.generateCategoryText(PostCategories.ERRANDS)}"),
                leading: CircleAvatar(
                  backgroundColor: Colors.grey,
                  child: Icon(
                    MaterialIcons.category,
                    color: Colors.white,
                  ),
                ),
                trailing: Container(
                  width: 16,
                  height: 16,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(3),
                      border: Border.all(
                          color: _applicationConstants.generateCategoryColor(PostCategories.ERRANDS),
                          width: 3
                      )
                  ),
                ),
              ),
              SizedBox(height: 10),
              ListTile(
                onTap: (){
                  _viewModel.setPostCategories(PostCategories.OTHER);
                  Navigator.pop(context);

                },
                title: Text("${_applicationConstants.generateCategoryText(PostCategories.OTHER)}"),
                leading: CircleAvatar(
                  backgroundColor: Colors.grey,
                  child: Icon(
                    MaterialIcons.category,
                    color: Colors.white,
                  ),
                ),
                trailing: Container(
                  width: 16,
                  height: 16,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(3),
                      border: Border.all(
                          color: _applicationConstants.generateCategoryColor(PostCategories.OTHER),
                          width: 3
                      )
                  ),
                ),
              ),
              SizedBox(height: 10),
            ],
          );
        }
    );
  }

  void selectOwner() {
    showModalBottomSheet(
        context: context,
        builder: (context){
          return Column(
            children: [
              SizedBox(height: 20),
              Center(
                child: Text(
                  "Görevli Seçin",
                  style: context.textTheme.headline4,
                ),
              ),

              Expanded(
                child: ListView.builder(
                  itemCount: _viewModel.allUserList.length,
                  itemBuilder: (context,index){
                    return ListTile(
                      onTap: (){
                        _viewModel.setOwner(_viewModel.allUserList[index]);
                        Navigator.pop(context);
                      },
                      title: Text(
                          "${_viewModel.allUserList[index]}"
                      ),
                      leading: CircleAvatar(
                        backgroundColor: Colors.grey,
                        child: Text(
                          "${_viewModel.allUserList[index].toUpperCase().substring(0,1)}",
                          style: GoogleFonts.roboto(
                            color: Colors.white,
                            fontWeight: FontWeight.bold
                          ),
                        ),
                      ),
                      trailing: Container(
                        width: 16,
                        height: 16,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(3),
                            border: Border.all(
                                color: _applicationConstants.generateCategoryColor(PostCategories.WORK),
                                width: 3
                            )
                        ),
                      ),
                    );
                  },
                ),
              )

            ],
          );
        }
    );
  }
}
