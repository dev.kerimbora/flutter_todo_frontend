import 'package:mobx/mobx.dart';
import 'package:todo/Core/Constants/application_constants.dart';
import 'package:todo/Core/Enums/post_categories.dart';
import 'package:todo/Services/HttpService/http_service.dart';
import 'package:todo/locator.dart';
part 'create_post_viewmodel.g.dart';

class CreatePostViewModel = _CreatePostViewModelBase with _$CreatePostViewModel;

abstract class _CreatePostViewModelBase with Store{

  HttpService _httpService = locator<HttpService>();
  ApplicationConstants _applicationConstants = ApplicationConstants.instance;

  @observable
  ObservableList<String> allUserList = ObservableList();


  String content = "";
  void setContent(String value) => content = value;

  @observable
  PostCategories postCategories = PostCategories.WORK;

  @action
  void setPostCategories(PostCategories value) => postCategories = value;

  @observable
  String owner = "";

  @action
  void setOwner(String value) => owner = value;

  @observable
  bool _loading = false;

  @computed
  bool get loading => _loading;

  @action
  Future<void> fetchAllUsers() async{
    _loading = true;
    List<String>? userList = await _httpService.getAllUserEmail();
    if(userList != null){
      allUserList.addAll(userList);
    }
    _loading = false;
  }

  @action
  Future<void> sendTodo(String currentUserEmail) async{
    _loading = true;
    Map<String,dynamic> body = {
      "content": content,
      "category": _applicationConstants.generateCategoryText(postCategories,forDb: true),
      "author": currentUserEmail,
      "owner": owner
    };
    await _httpService.createPost(body);
    _loading = false;
  }

}