// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'create_post_viewmodel.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$CreatePostViewModel on _CreatePostViewModelBase, Store {
  Computed<bool>? _$loadingComputed;

  @override
  bool get loading => (_$loadingComputed ??= Computed<bool>(() => super.loading,
          name: '_CreatePostViewModelBase.loading'))
      .value;

  final _$allUserListAtom = Atom(name: '_CreatePostViewModelBase.allUserList');

  @override
  ObservableList<String> get allUserList {
    _$allUserListAtom.reportRead();
    return super.allUserList;
  }

  @override
  set allUserList(ObservableList<String> value) {
    _$allUserListAtom.reportWrite(value, super.allUserList, () {
      super.allUserList = value;
    });
  }

  final _$postCategoriesAtom =
      Atom(name: '_CreatePostViewModelBase.postCategories');

  @override
  PostCategories get postCategories {
    _$postCategoriesAtom.reportRead();
    return super.postCategories;
  }

  @override
  set postCategories(PostCategories value) {
    _$postCategoriesAtom.reportWrite(value, super.postCategories, () {
      super.postCategories = value;
    });
  }

  final _$ownerAtom = Atom(name: '_CreatePostViewModelBase.owner');

  @override
  String get owner {
    _$ownerAtom.reportRead();
    return super.owner;
  }

  @override
  set owner(String value) {
    _$ownerAtom.reportWrite(value, super.owner, () {
      super.owner = value;
    });
  }

  final _$_loadingAtom = Atom(name: '_CreatePostViewModelBase._loading');

  @override
  bool get _loading {
    _$_loadingAtom.reportRead();
    return super._loading;
  }

  @override
  set _loading(bool value) {
    _$_loadingAtom.reportWrite(value, super._loading, () {
      super._loading = value;
    });
  }

  final _$fetchAllUsersAsyncAction =
      AsyncAction('_CreatePostViewModelBase.fetchAllUsers');

  @override
  Future<void> fetchAllUsers() {
    return _$fetchAllUsersAsyncAction.run(() => super.fetchAllUsers());
  }

  final _$sendTodoAsyncAction =
      AsyncAction('_CreatePostViewModelBase.sendTodo');

  @override
  Future<void> sendTodo(String currentUserEmail) {
    return _$sendTodoAsyncAction.run(() => super.sendTodo(currentUserEmail));
  }

  final _$_CreatePostViewModelBaseActionController =
      ActionController(name: '_CreatePostViewModelBase');

  @override
  void setPostCategories(PostCategories value) {
    final _$actionInfo = _$_CreatePostViewModelBaseActionController.startAction(
        name: '_CreatePostViewModelBase.setPostCategories');
    try {
      return super.setPostCategories(value);
    } finally {
      _$_CreatePostViewModelBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void setOwner(String value) {
    final _$actionInfo = _$_CreatePostViewModelBaseActionController.startAction(
        name: '_CreatePostViewModelBase.setOwner');
    try {
      return super.setOwner(value);
    } finally {
      _$_CreatePostViewModelBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
allUserList: ${allUserList},
postCategories: ${postCategories},
owner: ${owner},
loading: ${loading}
    ''';
  }
}
