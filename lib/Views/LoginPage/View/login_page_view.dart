import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_vector_icons/flutter_vector_icons.dart';
import 'package:todo/Core/Exceptions/user_exception.dart';
import 'package:todo/Core/Extensions/context_extensions.dart';
import 'package:todo/Views/HomePage/View/home_page_view.dart';
import 'package:todo/Views/LoginPage/ViewModel/login_page_viewmodel.dart';
import 'package:todo/Widgets/Bounce/bounce.dart';

class LoginPageView extends StatelessWidget {
  LoginPageView({Key? key}) : super(key: key);

  LoginPageViewModel _viewModel = LoginPageViewModel();

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
        appBar: buildAppBar(),
        body: buildBody(context),
      ),
    );
  }

  PreferredSize buildAppBar() {
    return PreferredSize(
      preferredSize: AppBar().preferredSize,
      child: Observer(
        builder:(_)=>AppBar(
          centerTitle: true,
          backgroundColor: _viewModel.loginMode ?Colors.redAccent : Colors.indigoAccent,
          title: Observer(
            builder: (_) {
              if(_viewModel.loginMode){
                return Text(
                  "Giriş",
                  style: TextStyle(fontWeight: FontWeight.w400),
                );
              }else{
                return Text(
                  "Kayıt Ol",
                  style: TextStyle(fontWeight: FontWeight.w400),
                );
              }
            },
          ),
          leading: SizedBox(),
        ),
      ),
    );
  }

  Widget buildBody(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        width: context.screenWidth,
        height: context.screenHeight - AppBar().preferredSize.height,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Observer(
              builder:(_)=>Icon(
                FontAwesome.user_circle_o,
                size: 100,
                color: _viewModel.loginMode?Colors.deepOrangeAccent:Colors.indigoAccent,
              ),
            ),
            SizedBox(height: 20),
            //Title
            Observer(builder:(_){
              if(_viewModel.loginMode){
                return Text("Giriş Yap", style: context.textTheme.headline3);
              }else{
                return Text("Kayıt Ol", style: context.textTheme.headline3);
              }
            }),
            SizedBox(height: 30),
            // Email Hint Text
            Container(
                width: context.screenWidth,
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 30),
                  child: Text(
                    "Email",
                    style: TextStyle(fontSize: 16, fontWeight: FontWeight.w600, color: Colors.grey.shade800),
                  ),
                )),
            SizedBox(height: 10),
            // Email Text Field
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 26),
              child: Container(
                width: context.screenWidth,
                height: 56,
                child: Card(
                  color: Colors.grey.shade200,
                  elevation: 3,
                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(6)),
                  child: TextField(
                    controller: _viewModel.emailController,
                    style: TextStyle(color: Colors.grey.shade700, fontSize: 16),
                    keyboardType: TextInputType.emailAddress,
                    decoration: InputDecoration(
                        prefixIcon: Icon(Icons.email, size: 28),
                        hintText: "example@email.com",
                        contentPadding: EdgeInsets.symmetric(vertical: 14, horizontal: 10)),
                  ),
                ),
              ),
            ),

            SizedBox(height: 30),
            // Password Hint Text
            Container(
                width: context.screenWidth,
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 30),
                  child: Text(
                    "Parola",
                    style: TextStyle(fontSize: 16, fontWeight: FontWeight.w600, color: Colors.grey.shade800),
                  ),
                )),
            SizedBox(height: 10),
            // Password Text Field
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 26),
              child: Container(
                width: context.screenWidth,
                height: 56,
                child: Card(
                  color: Colors.grey.shade200,
                  elevation: 3,
                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(6)),
                  child: TextField(
                    controller: _viewModel.passwordController,
                    style: TextStyle(color: Colors.grey.shade700, fontSize: 16),
                    obscureText: true,
                    keyboardType: TextInputType.emailAddress,
                    decoration: InputDecoration(
                        prefixIcon: Icon(Icons.lock, size: 28),
                        contentPadding: EdgeInsets.symmetric(vertical: 14, horizontal: 10)),
                  ),
                ),
              ),
            ),
            SizedBox(height: 50),

            // Login/Register Button
            Bounce(
              onPressed: ()async=>await loginAndRegisterButton(context),
              child: Observer(
                builder:(_)=>Container(
                  width: context.screenWidth / 1.5,
                  height: 40,
                  child: Center(
                    child: Observer(
                      builder: (_){
                        if(_viewModel.loading){
                          return Container(
                              width: 20,
                              height: 20,
                              child: CircularProgressIndicator(
                                valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
                              )
                          );
                        }else {
                          if (_viewModel.loginMode) {
                            return Text(
                              "Giriş Yap",
                              style: TextStyle(fontSize: 20, color: Colors.grey.shade200),
                            );
                          } else {
                            return Text(
                              "Kayıt Ol",
                              style: TextStyle(fontSize: 20, color: Colors.grey.shade200),
                            );
                          }
                        }
                      },
                    )
                  ),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(6),
                      color: _viewModel.loginMode? Colors.redAccent : Colors.indigoAccent
                  ),
                ),
              ),
            ),

            SizedBox(height: 10),
            // Divider
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 100),
              child: Divider(
                thickness: 1,
              ),
            ),

            // Login / Register Switch
            Observer(
              builder:(_)=>TextButton(
                onPressed: () =>  _viewModel.changeLoginMode(),
                child: Observer(
                  builder: (_){
                    if(_viewModel.loginMode){
                      return Text(
                        "Kayıt Ol",
                        style: TextStyle(fontSize: 20),
                      );
                    }else{
                      return Text(
                        "Giriş Yap",
                        style: TextStyle(fontSize: 20),
                      );
                    }
                  },
                ),
                style: TextButton.styleFrom(
                  primary: !_viewModel.loginMode? Colors.redAccent : Colors.indigoAccent
                ),
              ),
            ),
            SizedBox(height: 50),
          ],
        ),
      ),
    );
  }


  Future<void> loginAndRegisterButton(BuildContext context) async{
    if(_viewModel.loading) return;

    String email = _viewModel.emailController.text;
    String password = _viewModel.passwordController.text;

    bool emailValid = RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
        .hasMatch(email);

    if(!emailValid){
      showDialog(
          context: context,
          builder: (context){
            return AlertDialog(
              title: Text("Hata Oluştu"),
              content: Text("Geçerli Email Giriniz."),
              actions: [
                TextButton(onPressed: ()=>Navigator.pop(context), child: Text("Kapat"))
              ],
            );
          }
      );
      return;
    }

    if(password.length<=5){
      showDialog(
          context: context,
          builder: (context){
            return AlertDialog(
              title: Text("Hata Oluştu"),
              content: Text("Parolanız min 6 karakter olmalı!"),
              actions: [
                TextButton(onPressed: ()=>Navigator.pop(context), child: Text("Kapat"))
              ],
            );
          }
      );
      return;

    }


    if(_viewModel.loginMode) {
      try {
        await _viewModel.login();
        if(_viewModel.userModel != null) {
          MaterialPageRoute route = MaterialPageRoute(
              builder: (context) => HomePageView(userModel: _viewModel.userModel!));
          Navigator.of(context).push(route);
        }


      } catch (e) {
        _viewModel.setLoading(false);
        if(e is UserException){
          if(e.errorCode == 'user_eCode4'){
            showDialog(
                context: context,
                builder: (context){
                  return AlertDialog(
                    title: Text("Hata Oluştu"),
                    content: RichText(
                      text: TextSpan(
                          children: <TextSpan>[
                            TextSpan(
                                text: "Email veya Parola Hatalı veya Kullanıcı Bulunamadı\n\n",
                                style: TextStyle(
                                    fontSize: 16,
                                    color: Colors.grey.shade900
                                )
                            ),
                            TextSpan(
                                text: "Error Code : ${e.errorCode}",
                                style: TextStyle(
                                    fontSize: 12,
                                    color: Colors.grey.shade800
                                )
                            ),
                          ]
                      ),
                    ),
                    actions: [
                      TextButton(onPressed: ()=>Navigator.pop(context), child: Text("Kapat"))
                    ],
                  );
                }
            );
          }
        }else {
          print("[HATA] [LoginPageView] [buildBody] --> " + e.toString());
        }
      }

    } else{
      try {
        await _viewModel.register();
        if(_viewModel.userModel != null) {
          MaterialPageRoute route = MaterialPageRoute(
              builder: (context) => HomePageView(userModel: _viewModel.userModel!));
          Navigator.of(context).push(route);
        }


      } catch (e) {
        _viewModel.setLoading(false);
        if(e is UserException){
          if(e.errorCode == 'user_eCode3'){
            showDialog(
                context: context,
                builder: (context){
                  return AlertDialog(
                    title: Text("Hata Oluştu"),
                    content: RichText(
                      text: TextSpan(
                          children: <TextSpan>[
                            TextSpan(
                                text: "Kullanıcı Daha Önceden Kayıt Olmuş\n Lütfen Giriş Yapın\n\n",
                                style: TextStyle(
                                    fontSize: 16,
                                    color: Colors.grey.shade900
                                )
                            ),
                            TextSpan(
                                text: "Error Code : ${e.errorCode}",
                                style: TextStyle(
                                    fontSize: 12,
                                    color: Colors.grey.shade800
                                )
                            ),
                          ]
                      ),
                    ),
                    actions: [
                      TextButton(onPressed: ()=>Navigator.pop(context), child: Text("Kapat"))
                    ],
                  );
                }
            );
          }
        }else {
          print("[HATA] [LoginPageView] [buildBody] --> " + e.toString());
        }
      }

    }
  }

}
