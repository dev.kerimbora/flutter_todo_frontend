import 'package:flutter/cupertino.dart';
import 'package:mobx/mobx.dart';
import 'package:todo/Core/Exceptions/user_exception.dart';
import 'package:todo/Models/UserModel/user_model.dart';
import 'package:todo/Services/HttpService/http_service.dart';
import 'package:todo/locator.dart';
part 'login_page_viewmodel.g.dart';

class LoginPageViewModel = _LoginPageViewModelBase with _$LoginPageViewModel;

abstract class _LoginPageViewModelBase with Store{


  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  HttpService _httpService = locator<HttpService>();


  UserModel? userModel;

  @observable
  bool _loading = false;

  @computed
  bool get loading => _loading;

  @action
  void setLoading(bool value) => _loading = value;


  @observable
  bool _loginMode = true;

  @computed
  bool get loginMode => _loginMode;

  @action
  void changeLoginMode() {
    if(!loading) {
      _loginMode = !_loginMode;
    }
  }


  Future<void> login() async{

      _loading = true;
      userModel = await _httpService.loginUser(emailController.text, passwordController.text);
      _loading = false;

  }

  Future<void> register() async{

    _loading = true;
    userModel = await _httpService.registerUser(emailController.text, passwordController.text);
    _loading = false;
  }

}