// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'login_page_viewmodel.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$LoginPageViewModel on _LoginPageViewModelBase, Store {
  Computed<bool>? _$loadingComputed;

  @override
  bool get loading => (_$loadingComputed ??= Computed<bool>(() => super.loading,
          name: '_LoginPageViewModelBase.loading'))
      .value;
  Computed<bool>? _$loginModeComputed;

  @override
  bool get loginMode =>
      (_$loginModeComputed ??= Computed<bool>(() => super.loginMode,
              name: '_LoginPageViewModelBase.loginMode'))
          .value;

  final _$_loadingAtom = Atom(name: '_LoginPageViewModelBase._loading');

  @override
  bool get _loading {
    _$_loadingAtom.reportRead();
    return super._loading;
  }

  @override
  set _loading(bool value) {
    _$_loadingAtom.reportWrite(value, super._loading, () {
      super._loading = value;
    });
  }

  final _$_loginModeAtom = Atom(name: '_LoginPageViewModelBase._loginMode');

  @override
  bool get _loginMode {
    _$_loginModeAtom.reportRead();
    return super._loginMode;
  }

  @override
  set _loginMode(bool value) {
    _$_loginModeAtom.reportWrite(value, super._loginMode, () {
      super._loginMode = value;
    });
  }

  final _$_LoginPageViewModelBaseActionController =
      ActionController(name: '_LoginPageViewModelBase');

  @override
  void setLoading(bool value) {
    final _$actionInfo = _$_LoginPageViewModelBaseActionController.startAction(
        name: '_LoginPageViewModelBase.setLoading');
    try {
      return super.setLoading(value);
    } finally {
      _$_LoginPageViewModelBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void changeLoginMode() {
    final _$actionInfo = _$_LoginPageViewModelBaseActionController.startAction(
        name: '_LoginPageViewModelBase.changeLoginMode');
    try {
      return super.changeLoginMode();
    } finally {
      _$_LoginPageViewModelBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
loading: ${loading},
loginMode: ${loginMode}
    ''';
  }
}
