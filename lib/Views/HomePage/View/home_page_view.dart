import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_phoenix/flutter_phoenix.dart';
import 'package:flutter_vector_icons/flutter_vector_icons.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:todo/Core/Extensions/context_extensions.dart';
import 'package:todo/Models/UserModel/user_model.dart';
import 'package:todo/Views/CreateTodoPage/View/create_todo_page_view.dart';
import 'package:todo/Views/HomePage/ViewModel/home_page_viewmodel.dart';
import 'package:todo/Widgets/TodoCard/todo_card_widget.dart';

class HomePageView extends StatefulWidget {
  HomePageView({required this.userModel, Key? key}) : super(key: key);

  UserModel userModel;

  @override
  _HomePageViewState createState() => _HomePageViewState();
}

class _HomePageViewState extends State<HomePageView> {
  HomePageViewModel _viewModel = HomePageViewModel();

  @override
  void initState() {
    super.initState();

    _viewModel.fetchMyTodos(widget.userModel.email!);
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
        backgroundColor: Color(0xffe3e6e8),
        appBar: buildAppBar(context),
        body: buildBody(context),
      ),
    );
  }

  AppBar buildAppBar(BuildContext context) {
    return AppBar(
      centerTitle: true,
      leading: IconButton(
        icon: Icon(
            SimpleLineIcons.note,
          color: Colors.grey.shade700,
        ),
        onPressed: ()async{
          MaterialPageRoute<bool> route = MaterialPageRoute(
              builder: (context) => CreateTodoPageView(userModel: widget.userModel), fullscreenDialog: true);
          bool? postCreated = await Navigator.of(context).push(route);
          if(postCreated != null){
            if(postCreated){
              await _viewModel.refreshTodos(widget.userModel.email!);
            }
          }
        },
      ),
      actions: [
        IconButton(
          icon: Icon(
            Ionicons.person_circle,
            color: Colors.grey.shade700,
          ),
          onPressed: ()=>showUserInfoDialog(context),
        ),
      ],
      title: Text(
        "Flutter ToDo",
        style: TextStyle(
            color: Colors.grey.shade700,
            fontSize: 16
        ),
      ),
      backgroundColor: Colors.white,
      elevation: 0.3,
    );
  }

  Widget buildBody(BuildContext context) {
    return Stack(
      children: [
        Positioned.fill(
            child: Observer(
              builder:(_){
                if(_viewModel.loading){
                  return Center(
                    child: CircularProgressIndicator(),
                  );
                }else {
                  if(_viewModel.myTodos.isEmpty){
                    return RefreshIndicator(
                      onRefresh: () async {
                        await _viewModel.refreshTodos(widget.userModel.email!);
                      },
                      child: Container(
                        width: context.screenWidth,
                        height: context.screenHeight,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Icon(
                                Fontisto.slightly_smile,
                              size: 100,
                            ),
                            SizedBox(height: 20),
                            Text(
                                "Hiç Göreviniz Yok",
                              style: context.textTheme.headline4,
                            ),
                            SizedBox(height: 20),
                            ElevatedButton(
                                onPressed: ()=>_viewModel.refreshTodos(widget.userModel.email!),
                                child: Text("Yenile")
                            )
                          ],
                        ),
                      ),
                    );
                  }else {
                    return RefreshIndicator(
                      onRefresh: () async {
                        await _viewModel.refreshTodos(widget.userModel.email!);
                      },
                      child: Column(
                        children: [
                          Observer(
                            builder:(_)=>Visibility(
                              visible: _viewModel.isShowBanner,
                              child: MaterialBanner(
                                content: Text("Görevleri Silmek İçin Kaydırın"),
                                backgroundColor: Colors.grey.shade200,
                                actions: [
                                  TextButton(
                                      onPressed: ()=>_viewModel.setIsShowBanner(false),
                                      child: Text("Kapat"),
                                  )
                                ],
                              ),
                            ),
                          ),
                          Expanded(
                            child: ListView.builder(
                              physics: BouncingScrollPhysics(),
                              itemCount: _viewModel.myTodos.length+1,
                              itemBuilder: (context, index) {
                                if(index == _viewModel.myTodos.length) return SizedBox(height: context.screenHeight);
                                return Dismissible(
                                  key: UniqueKey(),
                                  background: Container(
                                    color: Colors.red,
                                    child: Center(
                                      child: Text(
                                        "Silmek İçin Kaydırın",
                                        style: GoogleFonts.roboto(
                                          fontSize: 22,
                                          color: Colors.white
                                        ),
                                      ),
                                    ),
                                  ),
                                  onDismissed: (dis) {
                                    print("Deleted : ${_viewModel.myTodos[index].id}");
                                    _viewModel.removeTodo(_viewModel.myTodos[index], index);
                                  },
                                  child: TodoCardWidget(todoModel: _viewModel.myTodos[index]),
                                );
                              },
                            ),
                          ),
                        ],
                      ),
                    );
                  }
                }
              },
            )
        ),
        Align(
          alignment: Alignment.bottomCenter,
          child: Padding(
            padding: const EdgeInsets.only(bottom: 30),
            child: GestureDetector(
              onTap: () async{
                MaterialPageRoute<bool> route = MaterialPageRoute(
                    builder: (context) => CreateTodoPageView(userModel: widget.userModel), fullscreenDialog: true);
                bool? postCreated = await Navigator.of(context).push(route);
                if(postCreated != null){
                  if(postCreated){
                    await _viewModel.refreshTodos(widget.userModel.email!);
                  }
                }
              },
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    width: context.screenWidth * 0.65,
                    height: 42,
                    child: Center(
                      child: Text("Yeni Bir Görev Yaz...",
                          textAlign: TextAlign.left,
                          style: GoogleFonts.montserrat(
                            fontSize: 16,
                            fontWeight: FontWeight.w300,
                            color: Colors.grey.shade200,
                          )),
                    ),
                    decoration: BoxDecoration(
                        color: Colors.grey.shade800,
                        borderRadius: BorderRadius.circular(20),
                        boxShadow: <BoxShadow>[
                          BoxShadow(
                            color: Colors.grey,
                            blurRadius: 4,
                            offset: Offset(2, 6),
                          )
                        ]),
                  ),
                  SizedBox(width: 10),
                  Container(
                    width: 42,
                    height: 42,
                    child: Center(
                      child: Icon(
                        SimpleLineIcons.note,
                        size: 18,
                        color: Colors.grey.shade200,
                      ),
                    ),
                    decoration: BoxDecoration(
                      color: Colors.grey.shade800,
                      shape: BoxShape.circle,
                        boxShadow: <BoxShadow>[
                          BoxShadow(
                            color: Colors.grey,
                            blurRadius: 4,
                            offset: Offset(2, 6),
                          )
                        ]
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }

  void showUserInfoDialog(context) {
    showDialog(
        context: context,
        builder: (context){
          return AlertDialog(
            title: Text("Profil"),
            contentPadding: EdgeInsets.only(top: 10),
            content: ListTile(
              onTap: (){},
              leading: CircleAvatar(
                child: Text(
                    "${widget.userModel.email!.toUpperCase().substring(0,1)}",
                  style: GoogleFonts.roboto(
                    fontWeight: FontWeight.bold,
                    color: Colors.white
                  ),
                ),
                backgroundColor: Colors.grey.shade800,
              ),
              title: Text(
                  "${widget.userModel.email!}",
                style: GoogleFonts.roboto(
                  fontSize: 14,
                  fontWeight: FontWeight.w300
                ),
              ),
            ),
            actions: [
              TextButton.icon(
                  onPressed: ()=>Phoenix.rebirth(context),
                  icon: Icon(MaterialCommunityIcons.exit_to_app),
                  label: Text("Çıkış Yap")
              )
            ],
          );
        },
    );
  }
}
