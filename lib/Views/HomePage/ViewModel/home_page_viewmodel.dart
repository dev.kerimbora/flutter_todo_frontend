import 'package:mobx/mobx.dart';
import 'package:todo/Models/TodoModel/todo_model.dart';
import 'package:todo/Services/HttpService/http_service.dart';
import 'package:todo/locator.dart';
part 'home_page_viewmodel.g.dart';

class HomePageViewModel = _HomePageViewModelBase with _$HomePageViewModel;

abstract class _HomePageViewModelBase with Store{

  HttpService _httpService = locator<HttpService>();

  @observable
  ObservableList<TodoModel> myTodos = ObservableList();

  @observable
  bool _loading = false;

  @computed
  bool get loading => _loading;

  @action
  Future<void> fetchMyTodos(String email) async{
    _loading = true;
    List<TodoModel>? todos = await _httpService.getMyTodos(email);
    if (todos != null) {
      todos.forEach((currentTodo) {
        myTodos.add(currentTodo);
      });
    }

    _loading = false;
  }

  @action
  Future<void> removeTodo(TodoModel todoModel,int index) async{
    myTodos.removeAt(index);
    if (todoModel.id != null) {
      await _httpService.removeTodo(todoModel.id!);
    }
  }

  @action
  Future<void> refreshTodos(String email)async{
    myTodos.clear();
    await fetchMyTodos(email);
  }


  @observable
  bool _isShowBanner = true;

  @computed
  bool get isShowBanner => _isShowBanner;

  @action
  void setIsShowBanner(bool value) => _isShowBanner = value;


}