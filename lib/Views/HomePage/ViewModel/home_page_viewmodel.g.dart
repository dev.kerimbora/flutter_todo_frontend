// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'home_page_viewmodel.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$HomePageViewModel on _HomePageViewModelBase, Store {
  Computed<bool>? _$loadingComputed;

  @override
  bool get loading => (_$loadingComputed ??= Computed<bool>(() => super.loading,
          name: '_HomePageViewModelBase.loading'))
      .value;
  Computed<bool>? _$isShowBannerComputed;

  @override
  bool get isShowBanner =>
      (_$isShowBannerComputed ??= Computed<bool>(() => super.isShowBanner,
              name: '_HomePageViewModelBase.isShowBanner'))
          .value;

  final _$myTodosAtom = Atom(name: '_HomePageViewModelBase.myTodos');

  @override
  ObservableList<TodoModel> get myTodos {
    _$myTodosAtom.reportRead();
    return super.myTodos;
  }

  @override
  set myTodos(ObservableList<TodoModel> value) {
    _$myTodosAtom.reportWrite(value, super.myTodos, () {
      super.myTodos = value;
    });
  }

  final _$_loadingAtom = Atom(name: '_HomePageViewModelBase._loading');

  @override
  bool get _loading {
    _$_loadingAtom.reportRead();
    return super._loading;
  }

  @override
  set _loading(bool value) {
    _$_loadingAtom.reportWrite(value, super._loading, () {
      super._loading = value;
    });
  }

  final _$_isShowBannerAtom =
      Atom(name: '_HomePageViewModelBase._isShowBanner');

  @override
  bool get _isShowBanner {
    _$_isShowBannerAtom.reportRead();
    return super._isShowBanner;
  }

  @override
  set _isShowBanner(bool value) {
    _$_isShowBannerAtom.reportWrite(value, super._isShowBanner, () {
      super._isShowBanner = value;
    });
  }

  final _$fetchMyTodosAsyncAction =
      AsyncAction('_HomePageViewModelBase.fetchMyTodos');

  @override
  Future<void> fetchMyTodos(String email) {
    return _$fetchMyTodosAsyncAction.run(() => super.fetchMyTodos(email));
  }

  final _$removeTodoAsyncAction =
      AsyncAction('_HomePageViewModelBase.removeTodo');

  @override
  Future<void> removeTodo(TodoModel todoModel, int index) {
    return _$removeTodoAsyncAction
        .run(() => super.removeTodo(todoModel, index));
  }

  final _$refreshTodosAsyncAction =
      AsyncAction('_HomePageViewModelBase.refreshTodos');

  @override
  Future<void> refreshTodos(String email) {
    return _$refreshTodosAsyncAction.run(() => super.refreshTodos(email));
  }

  final _$_HomePageViewModelBaseActionController =
      ActionController(name: '_HomePageViewModelBase');

  @override
  void setIsShowBanner(bool value) {
    final _$actionInfo = _$_HomePageViewModelBaseActionController.startAction(
        name: '_HomePageViewModelBase.setIsShowBanner');
    try {
      return super.setIsShowBanner(value);
    } finally {
      _$_HomePageViewModelBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
myTodos: ${myTodos},
loading: ${loading},
isShowBanner: ${isShowBanner}
    ''';
  }
}
