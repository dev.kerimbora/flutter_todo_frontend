
import 'package:todo/Models/TodoModel/todo_model.dart';
import 'package:todo/Models/UserModel/user_model.dart';

abstract class IHttpService{

  Future<UserModel?> loginUser(String email, String password);

  Future<UserModel?> registerUser(String email, String password);


  Future<List<TodoModel>?> getMyTodos(String email);

  Future<void> removeTodo(String todoId);


  Future<List<String>?> getAllUserEmail();

  Future<void> createPost(Map<String,dynamic> body);

}