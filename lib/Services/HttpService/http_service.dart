import 'dart:convert';

import 'package:todo/Core/Constants/application_constants.dart';
import 'package:todo/Core/Exceptions/todo_exception.dart';
import 'package:todo/Core/Exceptions/user_exception.dart';
import 'package:todo/Models/TodoModel/todo_model.dart';
import 'package:todo/Models/UserModel/user_model.dart';
import 'package:todo/Services/HttpService/i_http_service.dart';
import 'package:http/http.dart' as http;

class HttpService implements IHttpService{

  ApplicationConstants _applicationConstants = ApplicationConstants.instance;



  @override
  Future<UserModel?> loginUser(String email, String password) async{
      Uri baseUri = Uri.parse(_applicationConstants.API_BASE_URL+'/user/login');
      
      Map<String,dynamic> body = {
        "email": "$email",
        "password": "$password"
      };
      
      http.Response response = await http.post(
          baseUri,
          headers: {
            "Content-Type":"application/json"
          },
          body: jsonEncode(body)
      );

      if(response.statusCode == 200){
        dynamic body = jsonDecode(response.body);
        if(body is Map<String,dynamic>){
          UserModel userModel = UserModel.fromJson(body['userData']);
          return userModel;
        }else{
          print(body);
          return null;
        }
      }else if(response.statusCode == 403){
        dynamic body = jsonDecode(response.body);
        if(body is Map<String,dynamic>){
          throw UserException(body['e_code'], body['message']);
        }
        return null;
      }else{
        return null;
      }
  }

  @override
  Future<UserModel?> registerUser(String email, String password) async{
    Uri baseUri = Uri.parse(_applicationConstants.API_BASE_URL+'/user/register');

    Map<String,dynamic> body = {
      "email": "$email",
      "password": "$password"
    };

    http.Response response = await http.post(
        baseUri,
        headers: {
          "Content-Type":"application/json"
        },
        body: jsonEncode(body)
    );

    if(response.statusCode == 200){
      dynamic body = jsonDecode(response.body);
      if(body is Map<String,dynamic>){
        UserModel userModel = UserModel.fromJson(body['data']);
        return userModel;
      }else{
        print(body);
        return null;
      }
    }else if(response.statusCode == 403 || response.statusCode == 201){
      dynamic body = jsonDecode(response.body);
      if(body is Map<String,dynamic>){
        throw UserException(body['e_code'], body['message']);
      }
      return null;
    }else{
      return null;
    }
  }

  @override
  Future<List<TodoModel>?> getMyTodos(String email) async{
    Uri baseUri = Uri.parse(_applicationConstants.API_BASE_URL+'/todo/myTodos?email=$email');

    http.Response response = await http.get(
        baseUri,
        headers: {
          "Content-Type":"application/json"
        }
    );


    if(response.statusCode == 200){

      dynamic body = jsonDecode(response.body);

      if(body is List<dynamic>){
        List<TodoModel> myTodos = [];
        body.forEach((todoMap) {
          myTodos.add(TodoModel.fromJson(todoMap));
        });
        return myTodos;
      }else{

        print("body is not List<Map<String,dynamic>>");
        print(body);
        return null;

      }


    }else if(response.statusCode == 403){

      dynamic body = jsonDecode(response.body);
      if(body is Map<String,dynamic>){
        throw TodoException(body['e_code'], body['message']);
      }
      print("403 End [getMyTodos]");
      return null;

    }else{
      print(response.body);
      return null;
    }


  }

  @override
  Future<void> removeTodo(String todoId) async{
    try {
      Uri baseUri = Uri.parse(_applicationConstants.API_BASE_URL+'/todo/remove/$todoId');

      http.Response response = await http.delete(
          baseUri,
          headers: {
            "Content-Type":"application/json"
          }
      );

      return;
    } catch (e) {
      print("[HATA] [HttpService] [removeTodo] --> " + e.toString());
      return null;
    }
  }

  @override
  Future<List<String>?> getAllUserEmail() async{
    try {
      Uri baseUri = Uri.parse(_applicationConstants.API_BASE_URL+'/user');

       http.Response response = await http.get(
            baseUri,
            headers: {
             "Content-Type":"application/json"
           }
      );

       if(response.statusCode == 200){
         dynamic body = jsonDecode(response.body);
         List<String> users = [];

         if(body is List<dynamic>){
           body.forEach((email) {
             users.add(email);
           });
           return users;
         }else{

           print("[Get All Users] body is not List<dynamic>");
           print(body);
           return null;

         }
       }else{
         print("[HATA] [HttpService] [removeTodo] --> " + response.statusCode.toString());
         return null;
       }
    } catch (e) {
      print("[HATA] [HttpService] [getAllUserEmail] --> " + e.toString());
      return null;
    }
  }

  @override
  Future<void> createPost(Map<String,dynamic> body) async{
    try {
      Uri baseUri = Uri.parse(_applicationConstants.API_BASE_URL+'/todo/create');


      http.Response response = await http.post(
          baseUri,
          headers: {
            "Content-Type":"application/json"
          },
          body: jsonEncode(body)
      );
      return;
    } catch (e) {
      print("[HATA] [HttpService] [createPost] --> " + e.toString());
      return null;
    }
  }





}