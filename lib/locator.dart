

import 'package:get_it/get_it.dart';
import 'package:todo/Services/HttpService/http_service.dart';

final locator = GetIt.instance;


void setup() {
  locator.registerLazySingleton(() => HttpService());
}