

class UserModel{

  String? id;
  String? email;
  String? password;

  UserModel({
    required this.id,
    required this.email,
    required this.password
  });

  UserModel.fromJson(Map<String,dynamic> json):
      id = json['_id'],
      email = json['email'],
      password = json['password'];




}