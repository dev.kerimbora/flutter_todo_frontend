
class TodoModel{

  String? id;
  String? content;
  String? category;
  String? author;
  String? owner;
  int? createAt;

  TodoModel({
    required this.id,
    required this.content,
    required this.category,
    required this.author,
    required this.owner,
    required this.createAt
  });

  TodoModel.fromJson(dynamic json):
        id = json['_id'],
        content = json['content'],
        category = json['category'],
        author = json['author'],
        owner = json['owner'],
        createAt = json['created_at'];

}