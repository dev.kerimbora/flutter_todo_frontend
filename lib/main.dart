import 'package:flutter/material.dart';
import 'package:flutter_phoenix/flutter_phoenix.dart';
import 'package:todo/Core/Init/Theme/app_theme_light.dart';
import 'package:todo/Views/SplashScreen/View/splash_screen.dart';
import 'package:todo/locator.dart';

void main() {

  setup();

  runApp(
      Phoenix(
          child: TodoApp(),
      ),
  );
}

class TodoApp extends StatelessWidget {

  AppThemeLight _themeLight = AppThemeLight.instance;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'FlutterToDo',
      debugShowCheckedModeBanner: false,
      theme: _themeLight.theme,
      home: SplashScreen()
    );
  }


}