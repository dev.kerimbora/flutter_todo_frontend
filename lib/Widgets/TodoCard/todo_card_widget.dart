import 'package:flutter/material.dart';
import 'package:flutter_vector_icons/flutter_vector_icons.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:todo/Core/Constants/application_constants.dart';
import 'package:todo/Core/Extensions/context_extensions.dart';
import 'package:todo/Models/TodoModel/todo_model.dart';
import 'package:todo/Widgets/Bounce/bounce.dart';
import 'package:todo/Widgets/Bounce/bounce_without_hover.dart';

class TodoCardWidget extends StatelessWidget {
  TodoCardWidget({required this.todoModel,Key? key}) : super(key: key);

  TodoModel todoModel;
  ApplicationConstants _applicationConstants = ApplicationConstants.instance;


  @override
  Widget build(BuildContext context) {
    return BounceWithoutHover(
      onPressed: ()=>showTodoDetails(context),
      duration: Duration(milliseconds: 200),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 6,vertical: 2),
        child: Card(
          color: Colors.grey.shade100,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(12)
          ),
          elevation: 14,
          child: Row(
            children: [
              SizedBox(width: 20),
              Container(
                width: 12,
                height: 12,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(3),
                  border: Border.all(
                    color: _applicationConstants.generateCategoryColor(
                        _applicationConstants.decodePostCategory(todoModel.category!),
                    ),
                    width: 3
                  )
                ),
              ),
              SizedBox(width: 20),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.symmetric(vertical: 20),
                  child: Text(
                     "${todoModel.content}",
                    maxLines: 3,
                    textAlign: TextAlign.start,
                    overflow: TextOverflow.fade,
                    style: GoogleFonts.montserrat(
                      color: Colors.grey.shade800,
                    ),
                  ),
                ),
              ),
              CircleAvatar(
                radius: 14,
                backgroundColor: Colors.grey.shade800,
                child: Text(
                    "${todoModel.author!.toUpperCase().substring(0,1)}",
                  style: GoogleFonts.roboto(
                    fontWeight: FontWeight.bold,
                    fontSize: 14,
                    color: Colors.white
                  ),
                ),
              ),
              SizedBox(width: 10),
            ],
          ),
        ),
      ),
    );
  }


  void showTodoDetails(BuildContext context) {
    showModalBottomSheet(
        context: context,

        builder: (context){
          return Container(
            width: context.screenWidth,
            child: SingleChildScrollView(
              child: Column(
                children: [
                  //Title
                  SizedBox(height: 14),
                  Text(
                      "Görev Detayları",
                    style: GoogleFonts.montserrat(
                      fontSize: 22,
                      fontWeight: FontWeight.w600,
                      color: Colors.grey.shade800
                    ),
                  ),
                  SizedBox(height: 4),

                  // Öncelik
                  SizedBox(height: 14),
                  SizedBox(
                    width: context.screenWidth,
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 10,vertical: 2),
                      child: Text(
                        "Öncelik",
                        style: GoogleFonts.roboto(
                            fontSize: 16,
                            fontWeight: FontWeight.bold
                        ),
                      ),
                    ),
                  ),
                  SizedBox(height: 4),
                  ListTile(
                    onTap: (){},
                    contentPadding: EdgeInsets.symmetric(horizontal: 20),
                    minLeadingWidth: 1,
                    title: Text(
                        "${_applicationConstants.generateCategoryText(
                          _applicationConstants.decodePostCategory(todoModel.category!)
                        )}",
                      style: GoogleFonts.montserrat(
                        fontSize: 18,
                        fontWeight: FontWeight.w400
                      ),
                    ),
                    leading: Container(
                      width: 16,
                      height: 16,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(3),
                          border: Border.all(
                              color: _applicationConstants.generateCategoryColor(
                                _applicationConstants.decodePostCategory(todoModel.category!),
                              ),
                              width: 3
                          )
                      ),
                    ),
                  ),
                  Divider(height: 14),
                  // İçerik
                  SizedBox(
                    width: context.screenWidth,
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 10,vertical: 2),
                      child: Text(
                          "İçerik",
                        style: GoogleFonts.roboto(
                          fontSize: 16,
                          fontWeight: FontWeight.bold
                        ),
                      ),
                    ),
                  ),
                  SizedBox(height: 4),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 10,vertical: 2),
                    child: SizedBox(
                      width: context.screenWidth,
                      child: Text(
                        "${todoModel.content}",
                        textAlign: TextAlign.left,
                      ),
                    ),
                  ),
                  Divider(height: 14),
                  // Gönderen
                  SizedBox(
                    width: context.screenWidth,
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 10,vertical: 2),
                      child: Text(
                        "Gönderen",
                        style: GoogleFonts.roboto(
                            fontSize: 16,
                            fontWeight: FontWeight.bold
                        ),
                      ),
                    ),
                  ),
                  SizedBox(height: 4),
                  ListTile(
                    onTap: (){},
                    title: Text("${todoModel.author}"),
                    leading: CircleAvatar(
                      child: Text(
                        "${todoModel.author!.toUpperCase().substring(0,1)}",
                        style: GoogleFonts.roboto(
                            fontWeight: FontWeight.bold,
                            color: Colors.white
                        ),
                      ),
                      backgroundColor: Colors.grey.shade800,
                    ),
                  ),
                  Divider(height: 14),
                  // Gönderme Zamanı
                  SizedBox(
                    width: context.screenWidth,
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 10,vertical: 2),
                      child: Text(
                        "Gönderme Zamanı",
                        style: GoogleFonts.roboto(
                            fontSize: 16,
                            fontWeight: FontWeight.bold
                        ),
                      ),
                    ),
                  ),
                  SizedBox(height: 4),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 10,vertical: 2),
                    child: SizedBox(
                      width: context.screenWidth,
                      child: Text(
                        "${DateTime.fromMillisecondsSinceEpoch(todoModel.createAt!*1000).toLocal()}",
                        textAlign: TextAlign.left,
                      ),
                    ),
                  ),
                  SizedBox(height: 50)
                ],
              ),
            ),
          );
        }
    );
  }

}
