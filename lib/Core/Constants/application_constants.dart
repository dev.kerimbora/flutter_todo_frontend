
import 'package:flutter/material.dart';
import 'package:todo/Core/Enums/post_categories.dart';

class ApplicationConstants {
  static ApplicationConstants _instance = ApplicationConstants._init();
  static ApplicationConstants get instance => _instance;

  ApplicationConstants._init();


  String get API_BASE_URL => "https://flutter-todo-detaysoft.herokuapp.com";


  Color generateCategoryColor(PostCategories categories){
    switch(categories){
      case PostCategories.WORK:
        return Colors.deepPurple;
      case PostCategories.PERSONAL:
        return Colors.green;
      case PostCategories.ERRANDS:
        return Colors.deepOrange;
      case PostCategories.OTHER:
        return Colors.grey.shade600;
      default:
        return Colors.grey.shade600;
    }
  }

  String generateCategoryText(PostCategories categories,{bool? forDb}){
    switch(categories){
      case PostCategories.WORK:
        if(forDb != null){
          if(forDb){
            return "work";
          }
        }
        return "İş";
      case PostCategories.PERSONAL:
        if(forDb != null){
          if(forDb){
            return "personal";
          }
        }
        return "Kişisel";
      case PostCategories.ERRANDS:
        if(forDb != null){
          if(forDb){
            return "errands";
          }
        }
        return "Ayak İşleri";
      case PostCategories.OTHER:
        if(forDb != null){
          if(forDb){
            return "other";
          }
        }
        return "Kategorize Edilmemiş";
      default:
        if(forDb != null){
          if(forDb){
            return "other";
          }
        }
        return "Kategorize Edilmemiş";
    }
  }

  PostCategories decodePostCategory(String category){
    if(category == "work"){
      return PostCategories.WORK;
    }else if(category == "personal"){
      return PostCategories.PERSONAL;
    }else if(category == "errands"){
      return PostCategories.ERRANDS;
    }else{
      return PostCategories.OTHER;
    }
  }



}
