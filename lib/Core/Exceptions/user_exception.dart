
class UserException implements Exception{
  String errorCode;
  String message;

  UserException(this.errorCode, this.message);

  @override
  String toString() {
    return 'UserException{errorCode: $errorCode, message: $message}';
  }

}