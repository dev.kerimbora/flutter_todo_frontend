
class TodoException implements Exception{
  String errorCode;
  String message;

  TodoException(this.errorCode, this.message);

  @override
  String toString() {
    return 'TodoException{errorCode: $errorCode, message: $message}';
  }

}