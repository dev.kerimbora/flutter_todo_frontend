import 'package:flutter/material.dart';
import 'package:todo/Core/Init/Theme/i_app_theme.dart';

class AppThemeLight extends IAppTheme{
  static AppThemeLight _instance = AppThemeLight._init();
  static AppThemeLight get instance => _instance;

  AppThemeLight._init();

  ThemeData get theme => ThemeData(
    primaryColor: Colors.red,
    primarySwatch: Colors.deepOrange
  );

}